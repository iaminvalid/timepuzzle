﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PuzzleState : MonoBehaviour {

    public float timeScale;
    public GameObject returnButton;
    public GameObject tintObject;
    private float oldTimeScale;
    private TimeManager timeManager;
    public PuzzleStartButton curPuzzle;
    void Start()
    {
    timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
    }
    // Use this for initialization
    public void StartPuzzle()
    {
        returnButton.SetActive(true);
        oldTimeScale = timeManager.slider.value;
        timeManager.slider.value =  timeManager.AdjustFloat( timeScale);
        timeManager.slider.gameObject.SetActive(false);
        tintObject.SetActive(true);

    }
    public void EndPuzzle()
    {
        curPuzzle.isclicked = false;
        tintObject.SetActive(false);
        returnButton.SetActive(false);
        timeManager.slider.gameObject.SetActive(true);
        timeManager.slider.value = 1;
        gameObject.GetComponent<CameraMovement>().Return();

    }


}
