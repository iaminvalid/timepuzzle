﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class InputScript : MonoBehaviour {
    public GameObject SelectedMagicItem;
    public StreetCube streetCube;
    //public GameObject temp;
    public float zoomCap;
    private Vector3 curPos;
    private Vector3 startpos;
	// Use this for initialization
	void Start () {
        startpos = transform.position;
	}

    // Update is called once per frame
    void LateUpdate()
    {

        Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

        if (Input.GetMouseButtonDown(0))

        {


            // if left button pressed...
            if (Physics.Raycast(ray, out hit))  
            {
                if (hit.transform.tag == "CameraLocation")
                {
                    gameObject.GetComponent<PuzzleState>().curPuzzle = hit.transform.GetComponentInParent<PuzzleStartButton>();
                    gameObject.GetComponent<PuzzleState>().StartPuzzle();
                    hit.transform.parent.gameObject.GetComponent<PuzzleStartButton>().isclicked = true;

                }
                else if (hit.transform.tag == "Draggable")
                    hit.transform.GetComponent<MouseDrag>().StartClick();
                else if (hit.transform.tag == "Tappable")
                {
                    if (!hit.transform.gameObject.GetComponent<MagicItem>())
                        hit.transform.GetComponent<PuzzlePiece>().Tapped();
                    
                    else
                        if(hit.transform.GetComponent<MagicItem>() != null)
                            hit.transform.GetComponent<MagicItem>().PickUp();
                }

                return;
            }
            if (EventSystem.current.currentSelectedGameObject == null && hit.transform == null)
            {
                
                streetCube.StartClick();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.GetComponent<MagicItemPuzzlePiece>() != null)
                {
                    
                   if(hit.transform.GetComponent<MagicItemPuzzlePiece>().CheckCurrentItem(SelectedMagicItem))
                    SelectedMagicItem.GetComponentInParent<MagicItemUI>().DropItem();
                    SelectedMagicItem = null;
                    return;
                }
            }
            SelectedMagicItem = null;
        }
        //if (Input.touchCount == 2)
        //{
        //    temp.GetComponentInChildren<Text>().text = "two touch";
        //
        //    curPos = transform.position;
        //
        //    Touch tZero = Input.GetTouch(0);
        //    Touch tOne = Input.GetTouch(1);
        //
        //    Vector2 tZeroPrevPos = tZero.position - tZero.deltaPosition;
        //    Vector2 tOnePrevPos = tOne.position - tOne.deltaPosition;
        //
        //
        //    float prevTouchDeltaMag = (tZeroPrevPos = tOnePrevPos).magnitude;
        //    float touchDeltaMag = (tZero.position - tOne.position).magnitude;
        //
        //    float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;
        //
        //
        //    GetComponent<Camera>().fieldOfView += (deltaMagDiff ) * Time.unscaledDeltaTime;
        //
        //    if (GetComponent<Camera>().fieldOfView > 65 + zoomCap || GetComponent<Camera>().fieldOfView < 65 - zoomCap)
        //        GetComponent<Camera>().fieldOfView = GetComponent<Camera>().fieldOfView > 65 + zoomCap ? 65 + zoomCap : 65 - zoomCap;
        //    
        //
        //}
    }
}
