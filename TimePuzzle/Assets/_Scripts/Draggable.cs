﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


// Use this for initialization
public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    
    virtual public void OnBeginDrag(PointerEventData eventData)
    {
    }
    virtual public void OnDrag(PointerEventData eventData)
    {


    }
    virtual public void OnEndDrag(PointerEventData eventData)
    {

    }
}
