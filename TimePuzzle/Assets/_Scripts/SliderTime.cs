﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems; 


public class SliderTime : MonoBehaviour {
    public float curTime;
    public Slider slider;
    public bool tagged;
    private bool isBeingDragged;
	// Use this for initialization
	void Start () {
        curTime += Time.deltaTime;
        tagged = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!tagged)
        {

        curTime += Time.deltaTime;
        slider.value = curTime;

        }


    }
    
   
    
}
