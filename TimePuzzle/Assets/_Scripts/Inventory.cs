﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    public GameObject[] inventorySlots;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void UpdateInventory(int index)
    {
        inventorySlots[index].SetActive(true);
    }
}
