﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class apal : MonoBehaviour {
	public Animator myAnim;
	public Animator mainAnim;
	public GameObject rigidApple;
	public GameObject childApple;
	float speed;

	// Update is called once per frame
	void Update () {

		myAnim.SetFloat ("Speed", mainAnim.GetFloat ("Speed"));

		if (mainAnim.GetBool ("appleDropped"))
		{
			myAnim.SetBool ("appleDropped", true);
			rigidApple.SetActive (false);
			childApple.SetActive (true);
		}

	}
}
