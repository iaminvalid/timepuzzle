﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicItem : MonoBehaviour {
    public GameObject UICounterPart;
    private bool isOwned;
    private InputScript cameraInput;
    // Use this for initialization
	void Start () {
        cameraInput = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<InputScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PickUp()
    {
        isOwned = true;
        UICounterPart.SetActive(true);
        gameObject.SetActive(false);
    }
    public bool GetOwned()
    {
        return isOwned;
    }
}
