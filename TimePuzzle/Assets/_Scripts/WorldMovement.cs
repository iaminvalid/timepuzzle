﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StreetCube : MouseDrag{
    public float maxSpeed;
    public float minSpeed;
    public float maxYOffset;
    public float speedDropOff;
    
    private float curSpeed;
    private float slowTimer;
    private float Yposition;
    private Vector3 curPosition;
	// Use this for initialization
	void Start () {
        Yposition = transform.position.y;
        curPosition = transform.position;
	}

    // Update is called once per frame
    public override void Update() {
        if (isClicked)
        {

            curSpeed = (clickPosition.x - Input.mousePosition.x) /10;

            if (Mathf.Abs(curSpeed) > maxSpeed)
            {
                if (curSpeed < 0)
                    curSpeed = -maxSpeed;
                else
                    curSpeed = maxSpeed;
            }

            
            curPosition.y -= (clickPosition.y - Input.mousePosition.y) *Time.unscaledDeltaTime;

            if (curPosition.y > maxYOffset + Yposition || curPosition.y < Yposition - maxYOffset)
                curPosition.y = (curPosition.y > maxYOffset + Yposition) ? Yposition + maxYOffset : Yposition - maxYOffset;

            transform.position = curPosition;

            if (Mathf.Abs(curSpeed) > 1)
                transform.Rotate(Vector3.up * curSpeed * Time.unscaledDeltaTime);
            

        }
        else
        {
            curSpeed *= speedDropOff;
            if (Mathf.Abs(curSpeed) > minSpeed)
                transform.Rotate(Vector3.up * curSpeed * Time.unscaledDeltaTime);
        }






        if (Input.GetMouseButtonUp(0))
        {
            EndClick();
        }
    }

}
