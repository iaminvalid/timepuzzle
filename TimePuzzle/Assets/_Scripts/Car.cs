﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public float moveSpeed;
    public bool  wasTapped;
    private bool m_hit;
    private bool m_timerStart;
    public float collisionHitPower;
    private float m_sinTurn;
    public float turnDuration;
    private float m_turnTime;
    private bool m_returnDirection;
    private Vector3 m_curDirection;
    // Use this for initialization
    void Start()
    {
        m_sinTurn = 0;
        m_hit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!m_hit)
        {
            if (!wasTapped)
                transform.position += transform.forward * moveSpeed * Time.deltaTime;
            else
            {
                if (!m_returnDirection)
                    m_sinTurn += Time.deltaTime;
                else
                    m_sinTurn -= Time.deltaTime;

                if (m_sinTurn >= 1 && !m_returnDirection)
                {
                    m_timerStart = true;
                    m_sinTurn = 1;
                }
                else if (m_sinTurn <= 0 && m_returnDirection)
                {
                    wasTapped = false;
                    m_sinTurn = 0;
                }
                if (m_timerStart)
                {
                    m_turnTime += Time.deltaTime;
                    if (m_turnTime >= turnDuration)
                    {
                        m_timerStart = false;
                        m_returnDirection = true;
                    }
                }
                //m_curDirection = (transform.forward + (transform.right * Mathf.Sin(m_sinTurn))) * moveSpeed * Time.deltaTime;
                m_curDirection = transform.forward;
                m_curDirection += (transform.right * Mathf.Sin(m_sinTurn));
                m_curDirection = m_curDirection.normalized;
                m_curDirection *= moveSpeed * Time.deltaTime;
                transform.position += m_curDirection;

          
                
                
            }
        }
    }

    public Vector3 GetCurDirection()
    {
        return m_curDirection;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Rigidbody oRigid = collision.gameObject.GetComponent<Rigidbody>() ;
            oRigid.constraints = RigidbodyConstraints.None;
            oRigid.AddForce(transform.forward * collisionHitPower, ForceMode.Force);
            m_hit = true;
            collision.gameObject.GetComponent<ExplosionSpawner>().ExplodeCharacter(collision.transform.position, transform.rotation);

        }
    }
}
