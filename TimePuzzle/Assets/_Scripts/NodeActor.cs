﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeActor :  BaseActor{

    public GameObject NodePrefab;
    public GameObject StartNode;
    private GameObject curNode;
    private float curTime;



	// Use this for initialization
	void Start () {

        curTime = 0;
        curNode = StartNode;
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        startPos = gameObject.GetComponent<StartPosition>();
    }
	
	// Update is called once per frame
	override public void Update () {


        if (actorAnimator != null)
            UpdateAnimation();
        if (timeManager.curTime < 0)
            return;
        curTime = curNode.GetComponent<Node>().UpdateTime(curTime);
        transform.position = curNode.GetComponent<Node>().Move(curTime, transform.position);
        if (curNode.GetComponent<Node>().NextNode != null)
            transform.LookAt(curNode.GetComponent<Node>().NextNode.transform);
        curNode = curNode.GetComponent<Node>().transitionNode(curTime);
        

	}
   public  void addWaitNode(float duration)
    {

        timeManager.maxTime += duration;

        GameObject tempnode = Instantiate(NodePrefab,curNode.transform.parent);
        tempnode.transform.position = transform.position;
        tempnode.GetComponent<WaitNode>().waitDuration= duration;
        curNode.GetComponent<Node>().NewNode(tempnode,duration,timeManager.curTime );
        curNode = tempnode;
    }

}
