﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn : MonoBehaviour {
    private bool turning;
    public float turnDuration;
    private float turnTimer;

    private Quaternion previousRotation;
    private Quaternion targetDirection;

    private TimeManager timeManager;
    // Use this for initialization
    void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();

	}
	
	// Update is called once per frame
	void Update () {
        if (turning)
        {
            turnTimer += timeManager.AdjustFloat(Time.deltaTime);
            transform.rotation = Quaternion.Slerp(previousRotation, targetDirection, turnTimer);


            if (turnTimer < 0)
            {
                turning = false;
                transform.rotation = previousRotation;
            }
            if (turnTimer > 1)
            {
                turning = false;
                transform.rotation = targetDirection;
            }


        }
    }

    public void StartTurn(Quaternion direction)
    {
        if (timeManager.reverseTime)
            turnTimer = 1 - Time.deltaTime;
        else
            turnTimer = Time.deltaTime;
        turning = true;
        targetDirection = direction;
        previousRotation = transform.rotation;
    }
}
