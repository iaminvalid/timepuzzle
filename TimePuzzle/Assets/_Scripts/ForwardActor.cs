﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardActor : BaseActor {

    public bool waiting;
    private float waitTimer;
    private Quaternion TargetDirection;
    private Quaternion PreviousRotation;
    public float waitDuration;
    public float moveSpeed;
	// Use this for initialization
	void Start () {
        waitTimer = 0;
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();

    }

    // Update is called once per frame
    override public void Update () {

        UpdateAnimation();
        if (!waiting)
            transform.position += transform.forward * moveSpeed * timeManager.AdjustFloat(Time.deltaTime);
        else
        {
            waitTimer += timeManager.AdjustFloat(Time.deltaTime);
            if (waitTimer < 0 || waitTimer > waitDuration)
            {
                waiting = false;
                waitTimer = 0;
            }
        }





	}
    public void StartWait()
    {
        if (timeManager.reverseTime)
            waitTimer = waitDuration;
        else
            waitTimer = 0;
        waiting = true;
        
    }
    public void StartWait(float duration)
    {
        waiting = true;

        if (timeManager.reverseTime)
            waitTimer = duration;
        else
            waitTimer = 0;
    }

}
