﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
    public bool isStartNode = false;
    public bool isEndNode = false;
    public float endTime;
    public GameObject previousNode;
    public GameObject NextNode;
    protected TimeManager timeManager;
	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        if (!isEndNode)
            NextNode.GetComponent<Node>().previousNode = gameObject;
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    virtual public float UpdateTime(float curTime)
    {
        if (curTime < 0)
           return 0;
        if (curTime > timeManager.maxTime)
            return timeManager.maxTime;
        return curTime += timeManager.AdjustFloat(Time.deltaTime);
    }
    public GameObject GetNextNode()
    {
        return NextNode;
    }
    public GameObject GetPreviousNode()
    {
        return previousNode;
    }
    virtual public Vector3 Move(float curTime, Vector3 position)
    {
        if (isStartNode)
            return Vector3.Lerp(transform.position, NextNode.transform.position, curTime / endTime);
        else if (!isEndNode)
            return Vector3.Lerp(transform.position, NextNode.transform.position, (curTime - previousNode.GetComponent<Node>().endTime) / (endTime - previousNode.GetComponent<Node>().endTime));
        else if (isEndNode)
        {
            //check state of game and see if she is safe;
        }

        return transform.position;
        
    }
    virtual public GameObject transitionNode(float curTime)
    {
        if (!isEndNode && curTime > endTime)
            return GetNextNode();
        if (!isStartNode && curTime < previousNode.GetComponent<Node>().endTime)
            return GetPreviousNode();
        return gameObject;
    }
    public void NewNode(GameObject newNode,float duration, float curTime)
    {
        newNode.GetComponent<WaitNode>().previousNode = gameObject;
        newNode.GetComponent<WaitNode>().NextNode = NextNode;
        NextNode.GetComponent<Node>().previousNode = newNode;
        NextNode = newNode;

        newNode.GetComponent<WaitNode>().endTime = this.endTime;
        this.endTime = this.endTime - curTime;
        
       
    }
    public void UpdateOtherNodes(float duration)
    {
        this.endTime += duration;
        if (!isEndNode)
            NextNode.GetComponent<Node>().UpdateOtherNodes(duration);
    }
    public virtual void ItemChange(bool trigger)
    {

    }
}
