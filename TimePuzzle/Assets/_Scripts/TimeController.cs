﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TimeController : Draggable {
    private Vector3 prevPos;
    public float rotateSpeed;
    public override void OnBeginDrag(PointerEventData eventData)
    {
        prevPos = Input.mousePosition;
    }
    public override void OnDrag(PointerEventData eventData)
    {
        transform.Rotate(0, 0, Vector3.Distance(prevPos, Input.mousePosition) * Time.deltaTime);
        
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
    }

}
