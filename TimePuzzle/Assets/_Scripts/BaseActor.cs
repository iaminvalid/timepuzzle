﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseActor : MonoBehaviour {
    protected TimeManager timeManager;
    private float speed;
    public Animator actorAnimator;
    protected StartPosition startPos;

    // Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        startPos = gameObject.GetComponent<StartPosition>();
        Debug.Log(timeManager.tag);
    }

    // Update is called once per frame
    protected void UpdateAnimation ()
    {


        		if (timeManager.curTime == 0)
        			actorAnimator.speed = 0;
        		else
        			actorAnimator.speed = 1;
        //
        //        if (timeManager.reverseTime)
        //            speed = -2;
        //            else
        //            speed = 2;


        speed = timeManager.slider.value * 2;
        actorAnimator.SetBool("Reverse", timeManager.reverseTime);
        actorAnimator.SetFloat("Time", timeManager.curTime);
		actorAnimator.SetFloat("Speed", speed);

    }
    virtual public void Update()
    {

        UpdateAnimation();
    }
}
