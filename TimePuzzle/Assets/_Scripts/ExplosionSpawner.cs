﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpawner : MonoBehaviour {
    public GameObject explosionBlock;
    public float explosionPower;
    public float amountToSpawn;

    private Vector3 direction;
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExplodeCharacter(Vector3 position, Quaternion rotation)
    {
        for (int i = 0; i < amountToSpawn; i++)
        {
            Instantiate(explosionBlock, position , rotation);      
        }
        

    }
}
