﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tile : MonoBehaviour {
    public enum tileType
    {
        ROAD, BUILDING, GRASS

    }
    private GameObject nextTile;
    public tileType m_tileType;
	// Use this for initialization
	void Start () {
        
        if (m_tileType == tileType.ROAD)
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        if (m_tileType == tileType.BUILDING)
            gameObject.GetComponent<Renderer>().material.color = Color.grey;
        if (m_tileType == tileType.GRASS)
            gameObject.GetComponent<Renderer>().material.color = Color.green;
        Vector3 fwd = transform.TransformDirection(transform.forward);
        RaycastHit rayhit;
        if (Physics.Raycast(transform.position, fwd, out rayhit, 10))
        {
            if (rayhit.transform.gameObject.tag == "Tile")
            {
                nextTile = rayhit.transform.gameObject;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {


    }

}
