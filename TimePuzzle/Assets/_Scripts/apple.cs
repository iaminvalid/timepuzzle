﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class apple : MonoBehaviour {
    public GameObject mainActor;
    public Camera mainCam;
    public Animator mainAnim;
    public int puzzleIndex;
    public float waitDuration;
    private TimeManager timeManager;
	float speed;
	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
	}
	
	// Update is called once per frame
	void Update () {

		
	}
	private void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject.CompareTag("triggerAppleDrop"))
        {
            
            mainCam.GetComponent<CameraMovement>().Return();
            GameObject.FindGameObjectWithTag("TimeManager").GetComponent<PuzzleCompletionStore>().CompletePuzzle(0, timeManager.curTime, true);
            mainActor.GetComponentInParent<NodeActor>().addWaitNode(waitDuration);
        }
    }
}
