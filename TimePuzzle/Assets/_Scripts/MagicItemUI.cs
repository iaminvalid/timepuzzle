﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MagicItemUI : Draggable {
    private InputScript inputCamera;
    private Vector3 position;
    public GameObject WorldCounterPart;
    public GameObject PuzzlePiece;
    // Use this for initialization
	void Start () {
        position = GetComponent<RectTransform>().position;
        inputCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<InputScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void DropItem()
    {
        WorldCounterPart.SetActive(true);
        gameObject.SetActive(false);
    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        inputCamera.SelectedMagicItem = gameObject;
        transform.position = position;
        base.OnBeginDrag(eventData);

    }
    public override void OnDrag(PointerEventData eventData)
    {

        GetComponent<RectTransform>().position = Input.mousePosition;
        base.OnDrag(eventData);
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        RaycastHit hit;
        Ray ray = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        GetComponent<RectTransform>().position = position;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject == PuzzlePiece)
                DropItem();
        }
        
        base.OnEndDrag(eventData);
    }
}
