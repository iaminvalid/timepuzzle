﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManHole : MonoBehaviour {
    private bool playerFell;
    private bool RaisePlayer;
    public float fallDurtaion;
    public float raiseDuration;
    private float timer;
    private float triggerTimer;
    private GameObject actor;
    bool wasTriggered;
	// Use this for initialization
	void Start () {
        timer = 0;
        triggerTimer = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (wasTriggered)
        {
            triggerTimer += Time.deltaTime;
            if (triggerTimer >= 4)
            {
                wasTriggered = false;
                triggerTimer = 0;
            }
        }
            if (playerFell)
            {
                timer += Time.deltaTime;
                if (timer >= fallDurtaion)
                {
                    playerFell = false;
                    RaisePlayer = true;
                    timer = 0;
                    actor.transform.position = transform.position - new Vector3(0,1,0);
                    actor.GetComponent<Rigidbody>().useGravity = false;
                    actor.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
                    actor.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);


            }
        }
            if (RaisePlayer)
            {
                actor.transform.position += transform.up * ( Time.deltaTime / raiseDuration);
                if (actor.transform.position.y >= .5f)
                {

                    actor.GetComponent<CapsuleCollider>().enabled = true;
                    actor.GetComponent<BaseActor>().enabled = true;
                    actor.GetComponent<Rigidbody>().useGravity = true;
                }
            }
        


	}
    private void OnTriggerEnter(Collider other)
    {
        if (wasTriggered)
            return;
        if (other.gameObject.tag == "Player")
        {
            //do the pot hole animion;
            other.GetComponent<CapsuleCollider>().enabled = false ;
            other.GetComponent<BaseActor>().enabled = false;
            playerFell = true;
            actor = other.gameObject;
            wasTriggered = true;
        }
    }
}
