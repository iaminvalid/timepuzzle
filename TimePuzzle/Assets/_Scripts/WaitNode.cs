﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitNode : Node
{
    public float itemDurationChange;
    public float waitDuration;
    private float initalWaitDuration;
    private bool waitStarted;
    private float timer;
    private int nodeAction;

    void Start()
    {
        initalWaitDuration = waitDuration;
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        if (!isEndNode)
            NextNode.GetComponent<Node>().previousNode = gameObject;
    }
    public override Vector3 Move(float curTime, Vector3 position)
    {
        nodeAction = CheckWaitTimer(curTime);


        if (nodeAction == 1)
        {
            return base.Move(curTime, position);
        }
        
        return position;
    }
    public override GameObject transitionNode(float curTime)
    {

        switch (nodeAction)
        {
            case 0:
                return gameObject;
            case 1:
                GameObject temp = base.transitionNode(curTime);
                if (temp != gameObject)
                {
                    if (timer > waitDuration)
                        timer = waitDuration + endTime - previousNode.GetComponent<Node>().endTime;
                    return temp;
                }
                return temp;
            case 2:
                return ForceBack(curTime);
            default:
                return gameObject;
        }

    }
    private GameObject ForceBack(float curTime)
    {
        curTime = previousNode.GetComponent<Node>().endTime - Time.deltaTime;
        return base.transitionNode(curTime);
    }
    public override float UpdateTime(float curTime)
    {
        if (nodeAction == 1)
            return curTime + timeManager.AdjustFloat(Time.deltaTime);

        return curTime;
        
    }
    private int CheckWaitTimer(float curtime)
    {

        if (waitStarted)
            timer += timeManager.AdjustFloat(Time.deltaTime);

        if (timer > waitDuration)
        {
            return 1;
        }
        if (timer < 0)
        {
            if (!waitStarted)
                timer += timeManager.AdjustFloat(Time.deltaTime);
            else
                waitStarted = false;
            return 2;
        }
        if (!waitStarted)
            StartWait();
        return 0;
    }
    public void StartWait()
    {
        if (timeManager.reverseTime)
            timer = waitDuration;
        else
            timer = 0;
        waitStarted = true;
    }
    public override void ItemChange(bool trigger)
    {
        if (trigger)
        {
            waitDuration = initalWaitDuration + itemDurationChange;
            UpdateOtherNodes(itemDurationChange);
            timeManager.maxTime += itemDurationChange;
        }

        else
        {
            waitDuration = initalWaitDuration;
            UpdateOtherNodes(-itemDurationChange);
            timeManager.maxTime -= itemDurationChange;

        }


    }

}
