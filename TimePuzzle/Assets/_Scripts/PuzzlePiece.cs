﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour {
    public GameObject particle;
    public TimeManager timeManager;
    public PuzzleStartButton startButton;
    public PuzzlePiece pastPiece;
    public PuzzleStore puzzleStore;
    public Animator anim;
    public string animationName;
    public float animStartTime;
    private float animEndTime;

    public int value;
    public bool isSelected;

    public GameObject key;
	// Use this for initialization
	void Start () {

        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        RuntimeAnimatorController ac = anim.runtimeAnimatorController;

    


        for (int i = 0; i < ac.animationClips.Length; i++)
        {

            if (ac.animationClips[i].name == animationName)
                animEndTime = animStartTime + ac.animationClips[i].length;
            
        }
        
        

    }
	
	// Update is called once per frame
	void Update () {

        UpdateParticle();
        if (isSelected && timeManager.curTime > animStartTime && timeManager.curTime < animEndTime)
        {
            if(anim != null)
            anim.SetFloat("Speed", timeManager.slider.value);
        }
        if (timeManager.curTime > animEndTime || timeManager.curTime < animStartTime || !isSelected)
        {
            if(anim !=  null)
            anim.SetFloat("Speed", 0);
        }

        if (key != null)
        {

            if (isSelected && key != null)
            {
                if(!key.GetComponent<MagicItem>().GetOwned())
                    key.SetActive(true);
            }
        else
            key.SetActive(false);
        }
         
           

        if (GetComponent<Renderer>() == null)
            return;

        if (pastPiece != null)
            if (pastPiece.isSelected)
                GetComponent<Renderer>().material.color = Color.red;
            else
                GetComponent<Renderer>().material.color = Color.green;
        else
            GetComponent<Renderer>().material.color = Color.blue;
    }

    public virtual void Tapped()
    {
        if (pastPiece != null)
            if (!pastPiece.isSelected)
                return;

        if (startButton.isclicked)
        {
            startButton.UpdatePuzzle(value);
            isSelected = true;
        }
        if (anim != null)
            anim.Play(animationName);
        



    }
    public void UpdateSelected(bool canMove)
    {
        isSelected = canMove;
        if (anim != null)
        anim.SetBool("CanMove", canMove);
        
        
    }
    public void UpdateParticle()
    {

        if (startButton.isclicked)
            TurnOnParticle();
        particle.GetComponent<ParticleSystem>().Simulate(Time.unscaledDeltaTime,true,false);

    }
    public void TurnOnParticle()
    {
        particle.SetActive(true);
    }
    public void TurnOffParticle()
    {
        particle.SetActive(false);
    }
}
