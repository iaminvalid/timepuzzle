﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleStartButton : MonoBehaviour {
    public TimeManager animManager;
    public PuzzleStore puzzleStore;
    public PuzzleState puzzleState;
    public PuzzlePiece[] pieceArray;
    public float StartTime;
    public float EndTime;

    
    public int PuzzleIndex;

    public bool isclicked;

    private float curTime;
    // Use this for initialization
	void Start () {
        puzzleStore = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PuzzleStore>();
        puzzleState = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PuzzleState>();

    }

    // Update is called once per frame
    void Update () {

        curTime = animManager.curTime;

        if (curTime >= StartTime && curTime <= EndTime && !isclicked)
            transform.Find("ClockTarget").gameObject.SetActive(true) ;
        else
            transform.Find("ClockTarget").gameObject.SetActive(false);
		


    }

    public void UpdatePuzzle(int value)
    {
        puzzleStore.UpdatePuzzle(PuzzleIndex, value);
        for (int i = 0; i < pieceArray.Length; i++)
        {
            
            if (pieceArray[i].value != value)
            pieceArray[i].UpdateSelected(false);
            else
                pieceArray[i].UpdateSelected(true);

            pieceArray[i].TurnOffParticle();
        }

            isclicked = false;

            puzzleState.EndPuzzle();   
    }
}
