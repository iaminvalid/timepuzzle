﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPoint : MonoBehaviour {
    public GameObject otherCorner;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
    
        if (other.tag == "Player")
        {
                other.GetComponent<Turn>().StartTurn(otherCorner.transform.rotation);
        }
    }

}
