﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour {
    public GameObject cameraFocus;
    public bool isMoving;
    private float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(cameraFocus.transform, Vector3.up);
        if (!isMoving)
            return;
        
	}
    public void SetSpeed(float a_speed)
    {
        speed = a_speed;
    }
}
