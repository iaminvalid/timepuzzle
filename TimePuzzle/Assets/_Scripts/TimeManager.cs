﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {
    public Slider slider;
    public float curTime;
    public float maxTime;
    public float ResetDuration;
    public bool reverseTime;
    private float timeScale;
    // Use this for initialization
    void Start() {
        curTime = 0;
    }

    // Update is called once per frame
    void Update() {

  
			if (Input.GetKey(KeyCode.LeftArrow))
			{
				slider.value -= 2 * Time.unscaledDeltaTime;
			}
			if (Input.GetKey(KeyCode.RightArrow))
			{
				slider.value += 2 * Time.unscaledDeltaTime;
			}

            timeScale = slider.value;
        if (curTime >= maxTime && slider.value >0)
            timeScale = 0; 
        



            Time.timeScale = Mathf.Abs(timeScale);

            if (timeScale < 0)
            {
                curTime -= Time.deltaTime;
                if (curTime <= 0)
                    curTime = 0;
                
                reverseTime = true;
            }
            else
            {
                curTime += Time.deltaTime;
                reverseTime = false;
            }


            if (curTime > maxTime)
                curTime = maxTime;
        
    }

    public float AdjustFloat(float floatToAdjust)
    {
        if (!reverseTime)
            return floatToAdjust;
        else
            return -floatToAdjust;
    }
}
