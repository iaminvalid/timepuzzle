﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transparent : MonoBehaviour {
    public GameObject[] TransparentObject;
    public bool  m_isOn;
    private Color[] m_baseColour;
	// Use this for initialization
	void Start () {
        m_baseColour = new Color[TransparentObject.Length];
        for (int i = 0; i < TransparentObject.Length; i++)
        {
        m_baseColour[i] = TransparentObject[i].gameObject.GetComponent<MeshRenderer>().material.color;
            
        }
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < TransparentObject.Length; i++)
        {

            if (m_isOn)
                m_baseColour[i].a = 0.5f;
            if (!m_isOn)
                m_baseColour[i].a = 1;

                TransparentObject[i].gameObject.GetComponent<MeshRenderer>().material.color = m_baseColour[i]; 
        }
    }
}
