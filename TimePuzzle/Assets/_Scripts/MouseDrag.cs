﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour {

    public bool isClicked;
    public float moveSpeed;
    protected Vector3 clickPosition;
    protected Vector3 originPosition;
    void Start()
    {
        originPosition = transform.position;
    }
    // Update is called once per frame
    virtual public void Update () {
        if (isClicked)
        {
            Vector3 mousePos    = Input.mousePosition;
            Vector3 camSpacePos = Camera.main.WorldToScreenPoint(transform.position);
            mousePos.z = camSpacePos.z;

            Vector3 temppos = mousePos;
            temppos = Camera.main.ScreenToWorldPoint(temppos);
            

            transform.position = Vector3.Lerp( transform.position, temppos, Time.deltaTime * moveSpeed );
            
            
            
            if (Input.GetMouseButtonUp(0))
                EndClick();
        }
        
	}
    public void StartClick()
    {
        clickPosition = Input.mousePosition;
        originPosition = transform.position;
        isClicked = true;
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

    }
    protected void EndClick()
    {
        isClicked = false;
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

    }
}
