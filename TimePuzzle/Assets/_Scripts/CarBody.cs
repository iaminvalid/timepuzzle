﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBody : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 AdjustedDirection = Vector3.RotateTowards(transform.forward, GetComponentInParent<Car>().GetCurDirection(), 10, 0.0F);
        Quaternion targetRotation = Quaternion.LookRotation(AdjustedDirection.normalized);

        if (GetComponentInParent<Car>().wasTapped)
            transform.rotation = targetRotation;
        else
            transform.rotation = transform.parent.rotation;
	}
}
