﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicItemPuzzlePiece : PuzzlePiece {
    public Node NodeToChange;
    public GameObject magicItem;
    public bool wasTapped;
    private bool canTap;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateParticle();
        if (GetComponent<Renderer>() == null)
            return;

        if (magicItem.activeInHierarchy)
            GetComponent<Renderer>().material.color = Color.black;
        else
            GetComponent<Renderer>().material.color = Color.white;
	}
    public bool CheckCurrentItem(GameObject item)
    {
        if (!startButton.isclicked)
            return false;
        if (item == magicItem)
        {
            if (!canTap)
            {
                wasTapped = true;
            canTap = true;
            NodeToChange.ItemChange(true);
            }

            Tapped();
            if (wasTapped)
                return true;
            
        }
        return false;
        
    }
    public override  void Tapped()
    {
        
        if (canTap)
        {
            
            base.Tapped();
            canTap = false;
        }

    


    }
}
