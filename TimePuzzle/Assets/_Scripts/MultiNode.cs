﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiNode : Node {

    public GameObject[] possibleNodes = new GameObject[2];
    public bool firstNode;
        // Use this for initialization
	void Start () {
        NextNode = possibleNodes[0];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDirection(bool a_node)
    {
        firstNode = a_node;
        UpdateNode();
        
    }
    public void UpdateNode()
    {
        if (firstNode)
            NextNode = possibleNodes[0];
        else
            NextNode = possibleNodes[1];
    }

}
