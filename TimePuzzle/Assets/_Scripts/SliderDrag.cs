﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class SliderDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public GameObject slider;
    public SliderTime sliderTime;
    public TimeManager anim;
    private float xvalue;
    public void OnBeginDrag(PointerEventData eventData)
    {
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (false)
        {

        sliderTime.tagged = true ;
        slider.GetComponent<Slider>().value = 0;
        
        
        xvalue = eventData.position.x  -slider.transform.position.x + (slider.GetComponent<RectTransform>().rect.width / 2);
        Debug.Log(xvalue);
        if (xvalue > slider.GetComponent<RectTransform>().rect.width - 10)
            xvalue = slider.GetComponent<RectTransform>().rect.width - 10;
        if (xvalue < 0)
            xvalue = 0;

        transform.position  = slider.transform.position + new Vector3(xvalue,0,0);
        }
        Debug.Log("this one works");
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        sliderTime.tagged = false;
        slider.GetComponent<Slider>().value = xvalue;
        sliderTime.curTime = (xvalue / slider.GetComponent<Slider>().maxValue) * anim.maxTime;
        transform.position =  new Vector3(0, transform.position.y, transform.position.z);

    }
}
